package com.mnssfccarticle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oms2MwSfccarticleApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Oms2MwSfccarticleApiApplication.class, args);
	}

}
