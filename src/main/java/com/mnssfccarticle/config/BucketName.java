package com.mnssfccarticle.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BucketName {
    TODO_IMAGE("preprod-mw");
    private final String bucketName;
}