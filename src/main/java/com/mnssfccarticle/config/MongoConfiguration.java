package com.mnssfccarticle.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.CustomEditorConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.util.StringUtils;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Configuration
public class MongoConfiguration extends AbstractMongoClientConfiguration {
	 @Value("${environment}")
	 private String environment;
	 @Value("${mongo.dbname}")
	 private String dbname;
	 @Value("${mongo.certname}")
	 private String certname;
	 @Value("${mongo.certpass}")
	 private String certpass;
	 @Value("${mongo.username}")
	 private String username;
	 @Value("${mongo.password}")
	 private String password;
	 @Value("${mongo.host}")
	 private String host;
	 @Value("${mongo.port}")
	 private String port;
	
    @Override
    protected String getDatabaseName() {
        return dbname;
    }
    private SSLContext getNoopSslSocketFactory() {
        SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("SSL");
            File file = null;
    		try {
    			file = new ClassPathResource(certname).getFile();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		String path =file.getPath();
    		
    		/* File libfile = null;
    	 		try {
    	 			libfile = new ClassPathResource("config").getFile();
    	 			
    	 		} catch (IOException e) {
    	 			// TODO Auto-generated catch block
    	 			e.printStackTrace();
    	 		}
    	 		String libpath =libfile.getPath();
    		 System.setProperty("java.library.path", libpath);*/
            FileInputStream myKeys = new FileInputStream(path);
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(myKeys, certpass.toCharArray());
            kmf.init(keyStore, certpass.toCharArray());
            KeyManager[] keyManagers = kmf.getKeyManagers();
            sslContext.init(keyManagers, new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                }
                @Override
                public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                }
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return sslContext;
    }
    @Override
    public MongoClient mongoClient() {
    
     MongoCredential credential = MongoCredential.createCredential(username, dbname, password.toCharArray());
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
            //.applyConnectionString(connectionString)
            .credential(credential)
            .retryWrites(false)
            .applyToSslSettings(builder -> {
                builder.enabled(true);
                builder.invalidHostNameAllowed(true);
                builder.context(getNoopSslSocketFactory());
            })
            .applyToClusterSettings(builder -> 
            //builder.hosts(Arrays.asList(new ServerAddress("mns-dev-document-db.cluster-cdsdd8ajxxgo.ap-south-1.docdb.amazonaws.com", 27017))))
            builder.hosts(Arrays.asList(new ServerAddress(host, Integer.parseInt(port)))))
            .build();
        
        return MongoClients.create(mongoClientSettings);
    }
 
    
	
	}

