package com.mnssfccarticle.controllers;

import com.mnssfccarticle.model.SfccArt;
import com.mnssfccarticle.service.SfccArtService;
import lombok.AllArgsConstructor;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("api/")
@AllArgsConstructor
@CrossOrigin("*")
public class SfccArtController {
	SfccArtService service;

    @GetMapping
    public ResponseEntity<List<SfccArt>> getTodos() {
        return new ResponseEntity<>(service.getAllTodos(), HttpStatus.OK);
    }

    @PostMapping(
            path = "",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SfccArt> saveTodo(@RequestParam("title") String title,
                                         @RequestParam("description") String description,
                                         @RequestParam("file") MultipartFile file) {
        return new ResponseEntity<>(service.saveTodo(title, description, file), HttpStatus.OK);
    }

    @GetMapping(value = "pullCSV")
    public boolean downloadCSV() {
        return service.downloadCSV();
    }

    @GetMapping("/pushSfcc")
	public String getAccessToken(Pageable pageable) {
		String data = service.postSfccToMQ(pageable);
		return data;
	}
    @PostMapping("/pushSfccByMatnr")
	public String pushArticleByMatnr(@RequestBody String[] matnr) {
		String data = service.pushArticleByMatnr(matnr);
		return data;
	}
}