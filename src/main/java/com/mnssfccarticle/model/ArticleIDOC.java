package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ArticleIDOC {

	
	@JsonProperty("BEGIN")
	private String begin;
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WBB01")
	private List<E1WBB01> articles;
	@JsonProperty("EDI_DC40")
	private EDI_DC40 edidc40;
	private String status;
	
	
	
}
