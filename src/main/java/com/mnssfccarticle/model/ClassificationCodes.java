package com.mnssfccarticle.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class ClassificationCodes { 
	@JacksonXmlProperty(localName="TaxProductCode",isAttribute = true)
	public String TaxProductCode;

	@JsonProperty("TaxProductCode")
	public String getTaxProductCode() {
		return TaxProductCode;
	}

	public void setTaxProductCode(String taxProductCode) {
		TaxProductCode = taxProductCode;
	}

	public ClassificationCodes(String taxProductCode) {
		super();
		TaxProductCode = taxProductCode;
	}
	
	
}
