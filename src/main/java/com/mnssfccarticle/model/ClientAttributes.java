package com.mnssfccarticle.model;

import java.io.Serializable;

import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientAttributes implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String destination;
	private String lang;
	private String ashost;
	private String user;
	private String sysnr;
	private String password;
	private String client;
	private String codepage;
	private String poolCapacity;
	private String peakLimit;
	private String port;
}
