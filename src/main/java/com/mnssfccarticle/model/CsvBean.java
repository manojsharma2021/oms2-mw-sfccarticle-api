package com.mnssfccarticle.model;

import java.io.Serializable;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mnssfccarticle.model.SfccArt.SfccArtBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonPropertyOrder({"Article_ID", "Title", "Description","Product_Category","Product_Type","Link","Image_Link",
	"Availability","Retail_Price","Brand","EAN","Gender","Color","Size","Material","Pattern","Sales_Price",
	"ProductID","ProductType1","Deptartment_Number","Stroke","TNumber","Return_Window","UPC","HSN_Code",
	"ATS","Assorted","Is_Online","Online_From","Online_To","Creation_Date","Uploaded_to_Fluent","ReturnableItem"})
public class CsvBean implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/**
	 * 
	 */
    @JsonProperty("Article_ID")
	private String Article_ID;
    @JsonProperty("Title")
	private String Title;
    @JsonProperty("Description")
	private String Description;
    @JsonProperty("Product_Category")
	private String Product_Category;
    @JsonProperty("Product_Type")
	private String Product_Type;
    @JsonProperty("Link")
	private String Link;
    @JsonProperty("Image_Link")
	private String Image_Link;
    @JsonProperty("Availability")
	private String Availability;
    @JsonProperty("Retail_Price")
	private String Retail_Price;
    @JsonProperty("Brand")
	private String Brand;
    @JsonProperty("EAN")
	private String EAN;
    @JsonProperty("Gender")
	private String Gender;
    @JsonProperty("Color")
	private String Color;
    @JsonProperty("Size")
	private String Size;
    @JsonProperty("Material")
	private String Material;
    @JsonProperty("Pattern")
	private String Pattern;
    @JsonProperty("Sales_Price")
	private String Sales_Price;
    @JsonProperty("ProductID")
	private String ProductID;
    @JsonProperty("ProductType1")
	private String ProductType1;
    @JsonProperty("Deptartment_Number")
	private String Deptartment_Number;
    @JsonProperty("Stroke")
	private String Stroke;
    @JsonProperty("TNumber")
	private String TNumber;
    @JsonProperty("Return_Window")
	private String Return_Window;
    @JsonProperty("UPC")
	private String UPC;
    @JsonProperty("HSN_Code")
	private String HSN_Code;
    @JsonProperty("ATS")
	private String ATS;
    @JsonProperty("Assorted")
	private String Assorted;
    @JsonProperty("Is_Online")
	private String Is_Online;
    @JsonProperty("Online_From")
	private String Online_From;
    @JsonProperty("Online_To")
	private String Online_To;
    @JsonProperty("Creation_Date")
	private String Creation_Date;
    @JsonProperty("Uploaded_to_Fluent")
	private String Uploaded_to_Fluent;
    @JsonProperty("ReturnableItem")
	private String ReturnableItem;
	@Override
	public String toString() {
		return "CsvBean [Article_ID=" + Article_ID + ", Title=" + Title + ", Description=" + Description
				+ ", Product_Category=" + Product_Category + ", Product_Type=" + Product_Type + ", Link=" + Link
				+ ", Image_Link=" + Image_Link + ", Availability=" + Availability + ", Retail_Price=" + Retail_Price
				+ ", Brand=" + Brand + ", EAN=" + EAN + ", Gender=" + Gender + ", Color=" + Color + ", Size=" + Size
				+ ", Material=" + Material + ", Pattern=" + Pattern + ", Sales_Price=" + Sales_Price + ", ProductID="
				+ ProductID + ", Return_Window=" + Return_Window + ", ProductType1=" + ProductType1
				+ ", Deptartment_Number=" + Deptartment_Number + ", Stroke=" + Stroke + ", TNumber=" + TNumber
				+ ", UPC=" + UPC + ", HSN_Code=" + HSN_Code + ", ATS=" + ATS + ", Assorted=" + Assorted + ", Is_Online="
				+ Is_Online + ", Online_From=" + Online_From + ", Online_To=" + Online_To + ", Creation_Date="
				+ Creation_Date + ", Uploaded_to_Fluent=" + Uploaded_to_Fluent + ", ReturnableItem=" + ReturnableItem
				+ "]";
	}
	public String getArticle_ID() {
		return Article_ID;
	}
	public void setArticle_ID(String article_ID) {
		Article_ID = article_ID;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getProduct_Category() {
		return Product_Category;
	}
	public void setProduct_Category(String product_Category) {
		Product_Category = product_Category;
	}
	public String getProduct_Type() {
		return Product_Type;
	}
	public void setProduct_Type(String product_Type) {
		Product_Type = product_Type;
	}
	public String getLink() {
		return Link;
	}
	public void setLink(String link) {
		Link = link;
	}
	public String getImage_Link() {
		return Image_Link;
	}
	public void setImage_Link(String image_Link) {
		Image_Link = image_Link;
	}
	public String getAvailability() {
		return Availability;
	}
	public void setAvailability(String availability) {
		Availability = availability;
	}
	public String getRetail_Price() {
		return Retail_Price;
	}
	public void setRetail_Price(String retail_Price) {
		Retail_Price = retail_Price;
	}
	public String getBrand() {
		return Brand;
	}
	public void setBrand(String brand) {
		Brand = brand;
	}
	public String getEAN() {
		return EAN;
	}
	public void setEAN(String eAN) {
		EAN = eAN;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public String getColor() {
		return Color;
	}
	public void setColor(String color) {
		Color = color;
	}
	public String getSize() {
		return Size;
	}
	public void setSize(String size) {
		Size = size;
	}
	public String getMaterial() {
		return Material;
	}
	public void setMaterial(String material) {
		Material = material;
	}
	public String getPattern() {
		return Pattern;
	}
	public void setPattern(String pattern) {
		Pattern = pattern;
	}
	public String getSales_Price() {
		return Sales_Price;
	}
	public void setSales_Price(String sales_Price) {
		Sales_Price = sales_Price;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getProductType1() {
		return ProductType1;
	}
	public void setProductType1(String productType1) {
		ProductType1 = productType1;
	}
	public String getDeptartment_Number() {
		return Deptartment_Number;
	}
	public void setDeptartment_Number(String deptartment_Number) {
		Deptartment_Number = deptartment_Number;
	}
	public String getStroke() {
		return Stroke;
	}
	public void setStroke(String stroke) {
		Stroke = stroke;
	}
	public String getTNumber() {
		return TNumber;
	}
	public void setTNumber(String tNumber) {
		TNumber = tNumber;
	}
	public String getReturn_Window() {
		return Return_Window;
	}
	public void setReturn_Window(String return_Window) {
		Return_Window = return_Window;
	}
	public String getUPC() {
		return UPC;
	}
	public void setUPC(String uPC) {
		UPC = uPC;
	}
	public String getHSN_Code() {
		return HSN_Code;
	}
	public void setHSN_Code(String hSN_Code) {
		HSN_Code = hSN_Code;
	}
	public String getATS() {
		return ATS;
	}
	public void setATS(String aTS) {
		ATS = aTS;
	}
	public String getAssorted() {
		return Assorted;
	}
	public void setAssorted(String assorted) {
		Assorted = assorted;
	}
	public String getIs_Online() {
		return Is_Online;
	}
	public void setIs_Online(String is_Online) {
		Is_Online = is_Online;
	}
	public String getOnline_From() {
		return Online_From;
	}
	public void setOnline_From(String online_From) {
		Online_From = online_From;
	}
	public String getOnline_To() {
		return Online_To;
	}
	public void setOnline_To(String online_To) {
		Online_To = online_To;
	}
	public String getCreation_Date() {
		return Creation_Date;
	}
	public void setCreation_Date(String creation_Date) {
		Creation_Date = creation_Date;
	}
	public String getUploaded_to_Fluent() {
		return Uploaded_to_Fluent;
	}
	public void setUploaded_to_Fluent(String uploaded_to_Fluent) {
		Uploaded_to_Fluent = uploaded_to_Fluent;
	}
	public String getReturnableItem() {
		return ReturnableItem;
	}
	public void setReturnableItem(String returnableItem) {
		ReturnableItem = returnableItem;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

    
}
