package com.mnssfccarticle.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB01 {

	@Id
	private String eId;
	private String id;
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("LOCNR")
	private String locnr;
	@JsonProperty("MATNR")
	private String matnr;
	@JsonProperty("GLTAB")
	private String gtlab;
	@JsonProperty("AENKZ")
	private String aenkz;
	@JsonProperty("AENTP")
	private String aentp;
	@JsonProperty("SPRAS")
	private String spras;
	@JsonProperty("WAERS")
	private String waers;
	@JsonProperty("LANG_ISO")
	private String lang_iso;
	@JsonProperty("BSORT")
	private String bsort;
	@JsonProperty("SEG_REPEAT")
	private String seg_repeat;
	@JsonProperty("DYN_REDUC")
	private String dync_reduc;
	@JsonProperty("MATNR_LONG")
	private String matnr_long;
	
	@JsonProperty("ZE1WST01")
	private ZE1WST01 ze1wst01;
	
	@JsonProperty("ZE1WBB01")
	private ZE1WBB01 ze1wbb01;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="ZE1WBB01A")
	private List<ZE1WBB01A> ze1wbb01a;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="ZE1WBB21")
	private List<ZE1WBB21> ze1wbb21;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WBBCH")
	private List<E1WBBCH> e1wbbch;
	
	@JsonProperty("E1WBB03")
	private E1WBB03 e1wbb03;
	
	@JsonProperty("E1WBB02")
	private E1WBB02 e1wbb02;
	
	@JsonProperty("E1WBB09")
	private E1WBB09 e1wbb09;
	
	@JsonProperty("E1WBB10")
	private E1WBB10 e1wbb10;
	
	@JsonProperty("E1WBB16")
	private E1WBB16 e1wbb16;
	
	@JsonProperty("E1WBB12")
	private String e1wbb12;
	@JsonProperty("E1WBB13")
	private String e1wbb13;
	@JsonProperty("E1WBB14")
	private String e1wbb14;
	
	@JsonProperty("E1WBB15")
	private E1WBB15 e1wbb15;

	@JsonProperty("E1WBBAH")
	private E1WBBAH e1wbbah;
	
	@JsonProperty("E1WBB17")
	private String e1wbb17;
	@JsonProperty("E1WBB18")
	private String e1wbb18;
	@JsonProperty("E1WBB19")
	private String e1wbb19;
	
	@JsonProperty("E1WBB21")
	private String e1wbb21;
	@JsonProperty("E1WBB22")
	private String e1wbb22;
	private String status;
	private String serial;
	private String docnum;
	private Date creationDate;
	private Date updateDate;
	private String imageLink;
	private String imageId;
	private String color;
	private String size;
	private int sfccstatus;
}
