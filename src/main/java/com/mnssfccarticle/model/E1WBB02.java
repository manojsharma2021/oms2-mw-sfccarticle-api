package com.mnssfccarticle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB02 {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("LAEDA")
	private String laeda;
	@JsonProperty("LIQDT")
	private String liqdt;
	@JsonProperty("LVOMA")
	private String lvoma;
	@JsonProperty("BBTYP")
	private String bbtyp;
	@JsonProperty("VERNR")
	private String vernr;
	@JsonProperty("MTART")
	private String mtart;
	@JsonProperty("MATKL")
	private String matkl;
	@JsonProperty("ABTNR")
	private String abtnr;
	@JsonProperty("ATTYP")
	private String attyp;
	@JsonProperty("SATNR")
	private String satnr;
	@JsonProperty("BASME")
	private String basme;
	@JsonProperty("PRDHA")
	private String prdha;
	@JsonProperty("PLGTP")
	private String plgtp;
	@JsonProperty("SERVV")
	private String servv;
	@JsonProperty("SAISO")
	private String saiso;
	@JsonProperty("SAISJ")
	private String saisj;
	@JsonProperty("MHDHB")
	private String mhdhb;
	@JsonProperty("MHDRZ")
	private String mhdrz;
	@JsonProperty("MHDLP")
	private String mhdlp;
	@JsonProperty("MLGUT")
	private String mlgut;
	@JsonProperty("PRICE_BAND")
	private String price_band;
	@JsonProperty("PR_REF_MAT")
	private String pr_ref_mat;
	@JsonProperty("PLTYP_P")
	private String pltyp_p;
	@JsonProperty("IPRKZ")
	private String iprkz;
	@JsonProperty("NTGEW")
	private String ntgew;
	
	@JsonProperty("C_INFO_02")
	private String c_info_02;
	@JsonProperty("PR_REF_MAT_EXTERNAL")
	private String pr_ref_mat_external;
	@JsonProperty("PR_REF_MAT_VERSION")
	private String pr_ref_mat_version;
	@JsonProperty("PR_REF_MAT_GUID")
	private String pr_ref_mat_guid;
	@JsonProperty("SATNR_EXTERNAL")
	private String satnr_external;
	@JsonProperty("SATNR_VERSION")
	private String satnr_version;
	@JsonProperty("SATNR_GUID")
	private String satnr_guid;
	@JsonProperty("MARA_BSTME")
	private String mara_bstme;
	@JsonProperty("MARA_VABME")
	private String mara_vabme;
	@JsonProperty("MARA_INHME")
	private String mara_inhme;
	@JsonProperty("SATNR_LONG")
	private String satnr_long;
	@JsonProperty("PR_REF_MAT_LONG")
	private String pr_ref_mat_long;
	
	@JsonProperty("ZE1WBB02A")
	private ZE1WBB02A ze1wbb02a;
	
	@JsonProperty("ZE1WBB0BA")
	private ZE1WBB02B ze1wbb02b;

}
