package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB03 {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("MEINH")
	private String meinh;
	@JsonProperty("UMREZ")
	private String umrez;
	@JsonProperty("UMREN")
	private String umren;
	@JsonProperty("LAENG")
	private String laneg;
	@JsonProperty("BREIT")
	private String breit;
	@JsonProperty("HOEHE")
	private String hoehe;
	@JsonProperty("VOLUM")
	private String volum;
	@JsonProperty("BRGEW")
	private String brgew;
	@JsonProperty("GEWEI")
	private String gewei;
	@JsonProperty("MEABM")
	private String meabm;
	@JsonProperty("VOLEH")
	private String voleh;
	@JsonProperty("C_INFO_03")
	private String c_info_03;
	@JsonProperty("KZBSTME")
	private String kzbstme;
	@JsonProperty("KZAUSME")
	private String kzausme;
	
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WBB04")
	private List<E1WBB04> e1wbb04;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WBB07")
	private List<E1WBB07> e1wbb07;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WBB11")
	private List<E1WBB11> e1wbb11;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WBB20")
	private List<E1WBB20> e1wbb20;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WTADAL1")
	private List<E1WTADAL1> e1wtadal1;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WBBEM")
	private List<E1WBBEM> e1wbbem;
	
}
