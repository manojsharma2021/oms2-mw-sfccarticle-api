package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB04 {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("EAN11")
	private String ean11;
	@JsonProperty("EANTP")
	private String eantp;
	@JsonProperty("HPEAN")
	private String hpean;
	@JsonProperty("C_INFO_04")
	private String c_info_04;
	
	
}
