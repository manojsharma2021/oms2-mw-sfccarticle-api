package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB07 {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("KSCHL")
	private String kschl;
	@JsonProperty("DATAB")
	private String datab;
	@JsonProperty("STIME")
	private String stime;
	@JsonProperty("DATBI")
	private String datbi;
	@JsonProperty("ETIME")
	private String etime;
	@JsonProperty("AKTNR")
	private String aktnr;
	@JsonProperty("FVERW")
	private String fverw;
	@JsonProperty("STOCK_COUNT")
	private String stock_count;
	
	@JsonProperty("E1WBB08")
	private E1WBB08 e1wbb08;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="ZE1WBB07")
	private List<ZE1WBB07> ze1wbb07;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="ZE1WBB07A")
	private List<ZE1WBB07A> ze1wbb07a;

}
