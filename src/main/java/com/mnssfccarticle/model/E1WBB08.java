package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB08 {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("VORZN")
	private String vorzn;
	@JsonProperty("KWERT")
	private String kwert;
	@JsonProperty("KPEIN")
	private String kpein;
	@JsonProperty("KSATZ")
	private String ksatz;
	@JsonProperty("CURCY")
	private String curcy;
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WBBSC")
	private List<E1WBBSC> e1wbbsc;
	
	
}
