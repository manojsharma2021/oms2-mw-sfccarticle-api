package com.mnssfccarticle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB09 {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("VKDAB")
	private String vkdab;
	@JsonProperty("VKDBI")
	private String vkdbi;
	@JsonProperty("MSTDE")
	private String mstde;
	@JsonProperty("MSTDV")
	private String mstdv;
	@JsonProperty("PRDRU")
	private String prdru;
	@JsonProperty("ARANZ")
	private String aranz;
	@JsonProperty("KWDHT")
	private String kwdht;
	@JsonProperty("PRERF")
	private String prerf;
	@JsonProperty("DEINK")
	private String deink;
	@JsonProperty("SPVBC")
	private String spvbc;
	@JsonProperty("SPVBD")
	private String spvbd;
	@JsonProperty("RBZUL")
	private String rbzul;
	@JsonProperty("WAGAR")
	private String wagar;
	@JsonProperty("PRIMW")
	private String primw;
	@JsonProperty("PHFIL")
	private String phfil;
	
	@JsonProperty("WREPL")
	private String wrepl;
	@JsonProperty("WSTOR")
	private String wstor;
	@JsonProperty("SCAGR")
	private String scagr;
	@JsonProperty("WAUFT")
	private String wauft;
	@JsonProperty("C_INFO_09")
	private String c_info_09;
	
	@JsonProperty("ZE1WBB09")
	private ZE1WBB09 ze1wbb009;

}
