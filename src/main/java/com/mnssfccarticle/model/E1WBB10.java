package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB10 {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("MTXID")
	private String mtxid;
	@JsonProperty("MAKTM")
	private String maktm;
	@JsonProperty("C_INFO_10")
	private String c_info_10;
	
	
}
