package com.mnssfccarticle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB11 {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("AKTIO")
	private String aktio;
	@JsonProperty("VKDAB_A")
	private String vkdab_a;
	@JsonProperty("VKDBI_A")
	private String vkdbi_a;
	@JsonProperty("AKTHE")
	private String akthe;
	@JsonProperty("AKART")
	private String akart;
	@JsonProperty("AKTYP")
	private String aktyp;
	@JsonProperty("AUFME")
	private String aufme;
	@JsonProperty("AKLIZ")
	private String akliz;
	@JsonProperty("FBDAT")
	private String fbdat;
	@JsonProperty("SBDAT")
	private String sbdat;
	@JsonProperty("FLDAT")
	private String fldat;
	@JsonProperty("SLDAT")
	private String sldat;
	@JsonProperty("MIABG")
	private String miabg;
	@JsonProperty("NDISP")
	private String ndisp;
	@JsonProperty("RGART")
	private String rgart;
	@JsonProperty("PROM_TYPE")
	private String prom_type;

}
