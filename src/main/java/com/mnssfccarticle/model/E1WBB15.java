package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB15 {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("QUALMENGE")
	private String qualmenge;
	@JsonProperty("LOGMENGE")
	private String logmenge;
	@JsonProperty("LOGME")
	private String logme;
	@JsonProperty("LOGDATUM")
	private String logdatum;
	
	
}
