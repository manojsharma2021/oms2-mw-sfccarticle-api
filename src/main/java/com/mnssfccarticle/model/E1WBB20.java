package com.mnssfccarticle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBB20 {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("SPRAS_ME")
	private String spras_me;
	@JsonProperty("MTXID_ME")
	private String mtxid_me;
	@JsonProperty("LFDNR_ME")
	private String lfdnr_me;
	@JsonProperty("MAKTM_ME")
	private String maktm_me;
	@JsonProperty("LAISO_ME")
	private String laiso_me;
	@JsonProperty("C_INFO_20")
	private String c_info_20;

}
