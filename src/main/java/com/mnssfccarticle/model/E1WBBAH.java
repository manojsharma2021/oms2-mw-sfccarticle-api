package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBBAH {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("HIER_ID")
	private String hier_id;
	@JsonProperty("NODE")
	private String node;
	@JsonProperty("GLTAB")
	private String gltab;
	
	
}
