package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBBCH {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("CDCHGID")
	private String cdchgid;
	@JsonProperty("CHTAB")
	private String chtab;
	@JsonProperty("FLDNAME")
	private String fldname;
	@JsonProperty("TABKEY")
	private String tabkey;
	@JsonProperty("SEGTYP")
	private String segtyp;
	@JsonProperty("SEG_FLDNAME")
	private String seg_fldname;

}
