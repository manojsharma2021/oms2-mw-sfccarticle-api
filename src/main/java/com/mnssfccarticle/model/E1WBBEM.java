package com.mnssfccarticle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBBEM {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("AL_EMP_MATNR")
	private String al_emp_matnr;
	@JsonProperty("AL_EMP_QUANT")
	private String al_emp_quant;
	@JsonProperty("AL_EMP_UNIT")
	private String al_emp_unit;
	@JsonProperty("AL_EMP_MATNR_EXTERNAL")
	private String al_emp_matnr_external;
	@JsonProperty("AL_EMP_MATNR_VERSION")
	private String al_emp_matnr_version;
	@JsonProperty("AL_EMP_MATNR_GUID")
	private String al_emp_matnr_guid;
	@JsonProperty("AL_EMP_MATNR_LONG")
	private String al_emp_matnr_long;

}
