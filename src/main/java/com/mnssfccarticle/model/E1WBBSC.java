package com.mnssfccarticle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WBBSC {


	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("SCALE_TYPE")
	private String scale_type;
	@JsonProperty("SCABAS_VAL")
	private String scabas_val;
	@JsonProperty("SCABAS_QUAN")
	private String scabas_quan;
	@JsonProperty("SIGN")
	private String sign;
	@JsonProperty("COND_VALUE")
	private String cond_value;
}
