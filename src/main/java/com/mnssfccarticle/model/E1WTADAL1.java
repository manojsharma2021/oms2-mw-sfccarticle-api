package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WTADAL1 {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("ADDIMATNR")
	private String addimatnr;
	@JsonProperty("ADDIMAKTX")
	private String addimaktx;
	@JsonProperty("ADDIMATKL")
	private String addimatkl;
	@JsonProperty("ADDIMATKL_TXT")
	private String addimatkl_txt;
	@JsonProperty("ADDIFM")
	private String addifm;
	@JsonProperty("ADDIFM_TXT")
	private String addifm_txt;
	@JsonProperty("ADDIINVOC")
	private String addiinvoc;
	@JsonProperty("ADDIINHME")
	private String addiinhme;
	@JsonProperty("ADDIINHBR")
	private String addiinhbr;
	@JsonProperty("ADDIINHAL")
	private String addiinhal;
	@JsonProperty("ADDIVPREH")
	private String addivpreh;
	@JsonProperty("ADDINUM")
	private String addinum;
	@JsonProperty("ADDINUM_TXT")
	private String addinum_txt;
	@JsonProperty("ADDINUMBER")
	private String addinumber;
	@JsonProperty("ADDIREL")
	private String addirel;
	@JsonProperty("ADDIMATNR_EXTERNAL")
	private String addimatnr_external;
	@JsonProperty("ADDIMATNR_VERSION")
	private String addimatnr_version;
	@JsonProperty("ADDIMATNR_GUID")
	private String addimatnr_guid;
	@JsonProperty("ADDIMATNR_LONG")
	private String addimatnr_long;
	
	
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="E1WTADAL2")
	private List<E1WTADAL2> e1wtadal2;
}
