package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E1WTADAL2 {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("ADDISPRAS")
	private String addispras;
	@JsonProperty("ADDIROWNR")
	private String addirownr;
	@JsonProperty("ADDITXT")
	private String additxt;

}
