package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EDI_DC40 {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("TABNAM")
	private String tabnam;
	@JsonProperty("MANDT")
	private String mandt;
	@JsonProperty("DOCNUM")
	private String docnum;
	@JsonProperty("DOCREL")
	private String docrel;
	@JsonProperty("STATUS")
	private String status;
	@JsonProperty("DIRECT")
	private String direct;
	@JsonProperty("OUTMOD")
	private String outmod;
	@JsonProperty("IDOCTYP")
	private String idoctyp;
	@JsonProperty("MESTYP")
	private String mestyp;
	@JsonProperty("CIMTYP")
	private String cimtyp;
	@JsonProperty("SNDPOR")
	private String sndpor;
	@JsonProperty("SNDPRT")
	private String sndprt;
	@JsonProperty("SNDPRN")
	private String sndprn;
	@JsonProperty("RCVPOR")
	private String rcvpor;
	@JsonProperty("RCVPRT")
	private String rcvprt;
	@JsonProperty("RCVPRN")
	private String rcvprn;
	@JsonProperty("CREDAT")
	private String credat;
	@JsonProperty("CRETIM")
	private String cretim;
	@JsonProperty("SERIAL")
	private String serial;
	
	
}
