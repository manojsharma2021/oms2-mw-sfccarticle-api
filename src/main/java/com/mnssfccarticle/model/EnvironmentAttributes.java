package com.mnssfccarticle.model;

import java.io.Serializable;

import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EnvironmentAttributes implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String environment;
	private SAPAttributes sapArticle;
	private SAPAttributes sapTax;
	private SAPAttributes sapInvRes;
	private SAPAttributes sapDelivery;
	private SAPAttributes sapToConf;
	private SAPAttributes sapWmpgi;
	private SAPAttributes sapWmRetCanc;
	private SAPAttributes posdmSale;
	private SAPAttributes rraInvDotin;
	private SAPAttributes rraInvPpmp;
	
}
