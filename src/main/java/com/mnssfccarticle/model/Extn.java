package com.mnssfccarticle.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class Extn{ 
	@JacksonXmlProperty(localName="ExtnHSN",isAttribute = true)
	public String ExtnHSN;
	@JacksonXmlProperty(localName="ExtnStroke",isAttribute = true)
	public String ExtnStroke;
	@JacksonXmlProperty(localName="ExtnSalePrice",isAttribute = true)
	public String ExtnSalePrice;
	@JacksonXmlProperty(localName="ExtnIsOnSale",isAttribute = true)
	public String ExtnIsOnSale;
	@JacksonXmlProperty(localName="ExtnPriceBand",isAttribute = true)
	public String ExtnPriceBand;
	@JacksonXmlProperty(localName="ExtnSAPCat1Code",isAttribute = true)
	public String ExtnSAPCat1Code;
	@JacksonXmlProperty(localName="ExtnSAPCat2Code",isAttribute = true)
	public String ExtnSAPCat2Code;
	@JacksonXmlProperty(localName="ExtnSAPCat2Desc",isAttribute = true)
	public String ExtnSAPCat2Desc;
	@JacksonXmlProperty(localName="ExtnSAPCat3Code",isAttribute = true)
	public String ExtnSAPCat3Code;
	@JacksonXmlProperty(localName="ExtnSAPCat3Desc",isAttribute = true)
	public String ExtnSAPCat3Desc;
	@JacksonXmlProperty(localName="ExtnSAPCat4Code",isAttribute = true)
	public String ExtnSAPCat4Code;
	@JacksonXmlProperty(localName="ExtnSAPCat4Desc",isAttribute = true)
	public String ExtnSAPCat4Desc;
	@JacksonXmlProperty(localName="ExtnBrand",isAttribute = true)
	public String ExtnBrand;
	@JacksonXmlProperty(localName="ExtnDeptID",isAttribute = true)
	public String ExtnDeptID;
	@JacksonXmlProperty(localName="ExtnDeptDesc",isAttribute = true)
	public String ExtnDeptDesc;
	@JacksonXmlProperty(localName="ExtnTradeRange",isAttribute = true)
	public String ExtnTradeRange;
	@JacksonXmlProperty(localName="ExtnTradeSub",isAttribute = true)
	public String ExtnTradeSub;
	@JacksonXmlProperty(localName="ExtnCoreRange",isAttribute = true)
	public String ExtnCoreRange;
	@JacksonXmlProperty(localName="ExtnSFCCCat1Code",isAttribute = true)
	public String ExtnSFCCCat1Code;
	@JacksonXmlProperty(localName="ExtnSFCCCat2Code",isAttribute = true)
	public String ExtnSFCCCat2Code;
	@JacksonXmlProperty(localName="ExtnSFCCCat3Code",isAttribute = true)
	public String ExtnSFCCCat3Code;
	public Extn(String extnHSN, String extnStroke, String extnSalePrice, String extnIsOnSale, String extnPriceBand,
			String extnSAPCat1Code, String extnSAPCat2Code, String extnSAPCat2Desc, String extnSAPCat3Code,
			String extnSAPCat3Desc, String extnSAPCat4Code, String extnSAPCat4Desc, String extnBrand, String extnDeptID,
			String extnDeptDesc, String extnTradeRange, String extnTradeSub, String extnCoreRange,
			String extnSFCCCat1Code, String extnSFCCCat2Code, String extnSFCCCat3Code) {
		super();
		ExtnHSN = extnHSN;
		ExtnStroke = extnStroke;
		ExtnSalePrice = extnSalePrice;
		ExtnIsOnSale = extnIsOnSale;
		ExtnPriceBand = extnPriceBand;
		ExtnSAPCat1Code = extnSAPCat1Code;
		ExtnSAPCat2Code = extnSAPCat2Code;
		ExtnSAPCat2Desc = extnSAPCat2Desc;
		ExtnSAPCat3Code = extnSAPCat3Code;
		ExtnSAPCat3Desc = extnSAPCat3Desc;
		ExtnSAPCat4Code = extnSAPCat4Code;
		ExtnSAPCat4Desc = extnSAPCat4Desc;
		ExtnBrand = extnBrand;
		ExtnDeptID = extnDeptID;
		ExtnDeptDesc = extnDeptDesc;
		ExtnTradeRange = extnTradeRange;
		ExtnTradeSub = extnTradeSub;
		ExtnCoreRange = extnCoreRange;
		ExtnSFCCCat1Code = extnSFCCCat1Code;
		ExtnSFCCCat2Code = extnSFCCCat2Code;
		ExtnSFCCCat3Code = extnSFCCCat3Code;
	}
	@JsonProperty("ExtnHSN")
	public String getExtnHSN() {
		return ExtnHSN;
	}
	public void setExtnHSN(String extnHSN) {
		ExtnHSN = extnHSN;
	}
	@JsonProperty("ExtnStroke")
	public String getExtnStroke() {
		return ExtnStroke;
	}
	public void setExtnStroke(String extnStroke) {
		ExtnStroke = extnStroke;
	}
	@JsonProperty("ExtnSalePrice")
	public String getExtnSalePrice() {
		return ExtnSalePrice;
	}
	public void setExtnSalePrice(String extnSalePrice) {
		ExtnSalePrice = extnSalePrice;
	}
	@JsonProperty("ExtnIsOnSale")
	public String getExtnIsOnSale() {
		return ExtnIsOnSale;
	}
	public void setExtnIsOnSale(String extnIsOnSale) {
		ExtnIsOnSale = extnIsOnSale;
	}
	@JsonProperty("ExtnPriceBand")
	public String getExtnPriceBand() {
		return ExtnPriceBand;
	}
	public void setExtnPriceBand(String extnPriceBand) {
		ExtnPriceBand = extnPriceBand;
	}
	@JsonProperty("ExtnSAPCat1Code")
	public String getExtnSAPCat1Code() {
		return ExtnSAPCat1Code;
	}
	public void setExtnSAPCat1Code(String extnSAPCat1Code) {
		ExtnSAPCat1Code = extnSAPCat1Code;
	}
	@JsonProperty("ExtnSAPCat2Code")
	public String getExtnSAPCat2Code() {
		return ExtnSAPCat2Code;
	}
	public void setExtnSAPCat2Code(String extnSAPCat2Code) {
		ExtnSAPCat2Code = extnSAPCat2Code;
	}
	@JsonProperty("ExtnSAPCat2Desc")
	public String getExtnSAPCat2Desc() {
		return ExtnSAPCat2Desc;
	}
	public void setExtnSAPCat2Desc(String extnSAPCat2Desc) {
		ExtnSAPCat2Desc = extnSAPCat2Desc;
	}
	@JsonProperty("ExtnSAPCat3Code")
	public String getExtnSAPCat3Code() {
		return ExtnSAPCat3Code;
	}
	public void setExtnSAPCat3Code(String extnSAPCat3Code) {
		ExtnSAPCat3Code = extnSAPCat3Code;
	}
	@JsonProperty("ExtnSAPCat3Desc")
	public String getExtnSAPCat3Desc() {
		return ExtnSAPCat3Desc;
	}
	public void setExtnSAPCat3Desc(String extnSAPCat3Desc) {
		ExtnSAPCat3Desc = extnSAPCat3Desc;
	}
	@JsonProperty("ExtnSAPCat4Code")
	public String getExtnSAPCat4Code() {
		return ExtnSAPCat4Code;
	}
	public void setExtnSAPCat4Code(String extnSAPCat4Code) {
		ExtnSAPCat4Code = extnSAPCat4Code;
	}
	@JsonProperty("ExtnSAPCat4Desc")
	public String getExtnSAPCat4Desc() {
		return ExtnSAPCat4Desc;
	}
	public void setExtnSAPCat4Desc(String extnSAPCat4Desc) {
		ExtnSAPCat4Desc = extnSAPCat4Desc;
	}
	@JsonProperty("ExtnBrand")
	public String getExtnBrand() {
		return ExtnBrand;
	}
	public void setExtnBrand(String extnBrand) {
		ExtnBrand = extnBrand;
	}
	@JsonProperty("ExtnDeptID")
	public String getExtnDeptID() {
		return ExtnDeptID;
	}
	public void setExtnDeptID(String extnDeptID) {
		ExtnDeptID = extnDeptID;
	}
	@JsonProperty("ExtnDeptDesc")
	public String getExtnDeptDesc() {
		return ExtnDeptDesc;
	}
	public void setExtnDeptDesc(String extnDeptDesc) {
		ExtnDeptDesc = extnDeptDesc;
	}
	@JsonProperty("ExtnTradeRange")
	public String getExtnTradeRange() {
		return ExtnTradeRange;
	}
	public void setExtnTradeRange(String extnTradeRange) {
		ExtnTradeRange = extnTradeRange;
	}
	@JsonProperty("ExtnTradeSub")
	public String getExtnTradeSub() {
		return ExtnTradeSub;
	}
	public void setExtnTradeSub(String extnTradeSub) {
		ExtnTradeSub = extnTradeSub;
	}
	@JsonProperty("ExtnCoreRange")
	public String getExtnCoreRange() {
		return ExtnCoreRange;
	}
	public void setExtnCoreRange(String extnCoreRange) {
		ExtnCoreRange = extnCoreRange;
	}
	@JsonProperty("ExtnSFCCCat1Code")
	public String getExtnSFCCCat1Code() {
		return ExtnSFCCCat1Code;
	}
	public void setExtnSFCCCat1Code(String extnSFCCCat1Code) {
		ExtnSFCCCat1Code = extnSFCCCat1Code;
	}
	@JsonProperty("ExtnSFCCCat2Code")
	public String getExtnSFCCCat2Code() {
		return ExtnSFCCCat2Code;
	}
	public void setExtnSFCCCat2Code(String extnSFCCCat2Code) {
		ExtnSFCCCat2Code = extnSFCCCat2Code;
	}
	@JsonProperty("ExtnSFCCCat3Code")
	public String getExtnSFCCCat3Code() {
		return ExtnSFCCCat3Code;
	}
	public void setExtnSFCCCat3Code(String extnSFCCCat3Code) {
		ExtnSFCCCat3Code = extnSFCCCat3Code;
	}
	
	
	
}
