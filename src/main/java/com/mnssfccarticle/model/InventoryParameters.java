package com.mnssfccarticle.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class InventoryParameters { 
	@JacksonXmlProperty(localName="ATPRule",isAttribute = true)
	public String ATPRule;
	@JacksonXmlProperty(localName="InventoryMonitorRule",isAttribute = true)
	public String InventoryMonitorRule;
	public InventoryParameters(String aTPRule, String inventoryMonitorRule) {
		super();
		ATPRule = aTPRule;
		InventoryMonitorRule = inventoryMonitorRule;
	}
	@JsonProperty("ATPRule")
	public String getATPRule() {
		return ATPRule;
	}
	public void setATPRule(String aTPRule) {
		ATPRule = aTPRule;
	}
	@JsonProperty("InventoryMonitorRule")
	public String getInventoryMonitorRule() {
		return InventoryMonitorRule;
	}
	public void setInventoryMonitorRule(String inventoryMonitorRule) {
		InventoryMonitorRule = inventoryMonitorRule;
	}
	
	
}
