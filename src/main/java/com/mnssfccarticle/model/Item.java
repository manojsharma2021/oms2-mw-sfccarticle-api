package com.mnssfccarticle.model;

import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class Item{ 
	@JsonProperty("Extn")
	public Extn Extn;
	@JsonProperty("PrimaryInformation")
	public PrimaryInformation PrimaryInformation;
	@JsonProperty("InventoryParameters")
	public InventoryParameters InventoryParameters;
	@JsonProperty("ClassificationCodes")
	public ClassificationCodes ClassificationCodes;
	@JsonProperty("ItemAliasList")
	public ItemAliasList ItemAliasList;
	@JacksonXmlProperty(localName="ItemID",isAttribute = true)
	public String ItemID;
	@JacksonXmlProperty(localName="UnitOfMeasure",isAttribute = true)
	public String UnitOfMeasure;
	@JacksonXmlProperty(localName="OrganizationCode",isAttribute = true)
	public String OrganizationCode;
	@JacksonXmlProperty(localName="ItemGroupCode",isAttribute = true)
	public String ItemGroupCode;
	@JacksonXmlProperty(localName="Action",isAttribute = true)
	public String Action;
	@JacksonXmlProperty(localName="GlobalItemID",isAttribute = true)
	public String GlobalItemID;
	public Item(Extn extn, PrimaryInformation primaryInformation,
			InventoryParameters inventoryParameters,
			ClassificationCodes classificationCodes, ItemAliasList itemAliasList,
			String itemID, String unitOfMeasure, String organizationCode, String itemGroupCode, String action,
			String globalItemID) {
		super();
		Extn = extn;
		PrimaryInformation = primaryInformation;
		InventoryParameters = inventoryParameters;
		ClassificationCodes = classificationCodes;
		ItemAliasList = itemAliasList;
		ItemID = itemID;
		UnitOfMeasure = unitOfMeasure;
		OrganizationCode = organizationCode;
		ItemGroupCode = itemGroupCode;
		Action = action;
		GlobalItemID = globalItemID;
	}
	
	@JsonProperty("Extn")
	public Extn getExtn() {
		return Extn;
	}
	public void setExtn(Extn extn) {
		Extn = extn;
	}
	@JsonProperty("PrimaryInformation")
	public PrimaryInformation getPrimaryInformation() {
		return PrimaryInformation;
	}
	public void setPrimaryInformation(PrimaryInformation primaryInformation) {
		PrimaryInformation = primaryInformation;
	}
	@JsonProperty("InventoryParameters")
	public InventoryParameters getInventoryParameters() {
		return InventoryParameters;
	}
	public void setInventoryParameters(InventoryParameters inventoryParameters) {
		InventoryParameters = inventoryParameters;
	}
	@JsonProperty("ClassificationCodes")
	public ClassificationCodes getClassificationCodes() {
		return ClassificationCodes;
	}
	public void setClassificationCodes(ClassificationCodes classificationCodes) {
		ClassificationCodes = classificationCodes;
	}
	
	@JsonProperty("ItemAliasList")
	public ItemAliasList getItemAliasList() {
		return ItemAliasList;
	}
	public void setItemAliasList(ItemAliasList itemAliasList) {
		ItemAliasList = itemAliasList;
	}
	@JsonProperty("ItemID")
	public String getItemID() {
		return ItemID;
	}
	public void setItemID(String itemID) {
		ItemID = itemID;
	}
	@JsonProperty("UnitOfMeasure")
	public String getUnitOfMeasure() {
		return UnitOfMeasure;
	}
	public void setUnitOfMeasure(String unitOfMeasure) {
		UnitOfMeasure = unitOfMeasure;
	}
	@JsonProperty("OrganizationCode")
	public String getOrganizationCode() {
		return OrganizationCode;
	}
	public void setOrganizationCode(String organizationCode) {
		OrganizationCode = organizationCode;
	}
	@JsonProperty("ItemGroupCode")
	public String getItemGroupCode() {
		return ItemGroupCode;
	}
	public void setItemGroupCode(String itemGroupCode) {
		ItemGroupCode = itemGroupCode;
	}
	@JsonProperty("Action")
	public String getAction() {
		return Action;
	}
	public void setAction(String action) {
		Action = action;
	}
	@JsonProperty("GlobalItemID")
	public String getGlobalItemID() {
		return GlobalItemID;
	}
	public void setGlobalItemID(String globalItemID) {
		GlobalItemID = globalItemID;
	}
	
	
}
