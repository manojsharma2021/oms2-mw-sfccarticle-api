package com.mnssfccarticle.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class ItemAlias{ 
	@JacksonXmlProperty(localName="Action",isAttribute = true)
	public String Action;
	@JacksonXmlProperty(localName="AliasName",isAttribute = true)
	public String AliasName;
	@JacksonXmlProperty(localName="AliasValue",isAttribute = true)
	public String AliasValue;
	public ItemAlias(String action, String aliasName, String aliasValue) {
		super();
		Action = action;
		AliasName = aliasName;
		AliasValue = aliasValue;
	}
	@JsonProperty("Action")
	public String getAction() {
		return Action;
	}
	public void setAction(String action) {
		Action = action;
	}
	@JsonProperty("AliasName")
	public String getAliasName() {
		return AliasName;
	}
	public void setAliasName(String aliasName) {
		AliasName = aliasName;
	}
	@JsonProperty("AliasValue")
	public String getAliasValue() {
		return AliasValue;
	}
	public void setAliasValue(String aliasValue) {
		AliasValue = aliasValue;
	}
	
	
}
