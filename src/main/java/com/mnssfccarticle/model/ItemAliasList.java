package com.mnssfccarticle.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class ItemAliasList { 
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="ItemAlias")
	public List<ItemAlias> ItemAlias;

	public ItemAliasList(List<ItemAlias> itemAlias) {
		super();
		ItemAlias = itemAlias;
	}

	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="ItemAlias")
	public List<ItemAlias> getItemAlias() {
		return ItemAlias;
	}

	public void setItemAlias(List<ItemAlias> itemAlias) {
		ItemAlias = itemAlias;
	}
	
	
}
