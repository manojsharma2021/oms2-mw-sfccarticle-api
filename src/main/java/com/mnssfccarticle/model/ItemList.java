package com.mnssfccarticle.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ItemList { 
	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="Item")
	public ArrayList<Item> items;

	@JacksonXmlElementWrapper(useWrapping = false)   
	@JacksonXmlProperty(localName="Item")
	public ArrayList<Item> getItems() {
		return items;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	public ItemList(ArrayList<Item> items) {
		super();
		this.items = items;
	}
	
	
}
