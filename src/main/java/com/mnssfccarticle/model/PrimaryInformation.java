package com.mnssfccarticle.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class PrimaryInformation { 
	@JacksonXmlProperty(localName="Status",isAttribute = true)
	public String Status;
	@JacksonXmlProperty(localName="ShortDescription",isAttribute = true)
	public String ShortDescription;
	@JacksonXmlProperty(localName="Description",isAttribute = true)
	public String Description;
	@JacksonXmlProperty(localName="ExtendedDescription",isAttribute = true)
	public String ExtendedDescription;
	@JacksonXmlProperty(localName="ReturnWindow",isAttribute = true)
	public String ReturnWindow;
	@JacksonXmlProperty(localName="ProductLine",isAttribute = true)
	public String ProductLine;
	@JacksonXmlProperty(localName="IsShippingAllowed",isAttribute = true)
	public String IsShippingAllowed;
	@JacksonXmlProperty(localName="IsProcurementAllowed",isAttribute = true)
	public String IsProcurementAllowed;
	@JacksonXmlProperty(localName="IsParcelShippingAllowed",isAttribute = true)
	public String IsParcelShippingAllowed;
	@JacksonXmlProperty(localName="IsDeliveryAllowed",isAttribute = true)
	public String IsDeliveryAllowed;
	@JacksonXmlProperty(localName="AllowGiftWrap",isAttribute = true)
	public String AllowGiftWrap;
	@JacksonXmlProperty(localName="CountryOfOrigin",isAttribute = true)
	public String CountryOfOrigin;
	@JacksonXmlProperty(localName="TaxableFlag",isAttribute = true)
	public String TaxableFlag;
	@JacksonXmlProperty(localName="UnitCost",isAttribute = true)
	public String UnitCost;
	@JacksonXmlProperty(localName="DefaultProductClass",isAttribute = true)
	public String DefaultProductClass;
	@JacksonXmlProperty(localName="ItemType",isAttribute = true)
	public String ItemType;
	@JacksonXmlProperty(localName="IsReturnable",isAttribute = true)
	public String IsReturnable;
	@JacksonXmlProperty(localName="ColorCode",isAttribute = true)
	public String ColorCode;
	@JacksonXmlProperty(localName="SizeCode",isAttribute = true)
	public String SizeCode;
	@JacksonXmlProperty(localName="ImageID",isAttribute = true)
	public String ImageID;
	@JacksonXmlProperty(localName="ImageLocation",isAttribute = true)
	public String ImageLocation;
	public PrimaryInformation(String status, String shortDescription, String description, String extendedDescription,
			String returnWindow, String productLine, String isShippingAllowed, String isProcurementAllowed,
			String isParcelShippingAllowed, String isDeliveryAllowed, String allowGiftWrap, String countryOfOrigin,
			String taxableFlag, String unitCost, String defaultProductClass, String itemType, String isReturnable,
			String colorCode, String sizeCode, String imageID, String imageLocation) {
		super();
		Status = status;
		ShortDescription = shortDescription;
		Description = description;
		ExtendedDescription = extendedDescription;
		ReturnWindow = returnWindow;
		ProductLine = productLine;
		IsShippingAllowed = isShippingAllowed;
		IsProcurementAllowed = isProcurementAllowed;
		IsParcelShippingAllowed = isParcelShippingAllowed;
		IsDeliveryAllowed = isDeliveryAllowed;
		AllowGiftWrap = allowGiftWrap;
		CountryOfOrigin = countryOfOrigin;
		TaxableFlag = taxableFlag;
		UnitCost = unitCost;
		DefaultProductClass = defaultProductClass;
		ItemType = itemType;
		IsReturnable = isReturnable;
		ColorCode = colorCode;
		SizeCode = sizeCode;
		ImageID = imageID;
		ImageLocation = imageLocation;
	}
	@JsonProperty("Status")
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	@JsonProperty("ShortDescription")
	public String getShortDescription() {
		return ShortDescription;
	}
	public void setShortDescription(String shortDescription) {
		ShortDescription = shortDescription;
	}
	@JsonProperty("Description")
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	@JsonProperty("ExtendedDescription")
	public String getExtendedDescription() {
		return ExtendedDescription;
	}
	public void setExtendedDescription(String extendedDescription) {
		ExtendedDescription = extendedDescription;
	}
	@JsonProperty("ReturnWindow")
	public String getReturnWindow() {
		return ReturnWindow;
	}
	public void setReturnWindow(String returnWindow) {
		ReturnWindow = returnWindow;
	}
	@JsonProperty("ProductLine")
	public String getProductLine() {
		return ProductLine;
	}
	public void setProductLine(String productLine) {
		ProductLine = productLine;
	}
	@JsonProperty("IsShippingAllowed")
	public String getIsShippingAllowed() {
		return IsShippingAllowed;
	}
	public void setIsShippingAllowed(String isShippingAllowed) {
		IsShippingAllowed = isShippingAllowed;
	}
	@JsonProperty("IsProcurementAllowed")
	public String getIsProcurementAllowed() {
		return IsProcurementAllowed;
	}
	public void setIsProcurementAllowed(String isProcurementAllowed) {
		IsProcurementAllowed = isProcurementAllowed;
	}
	@JsonProperty("IsParcelShippingAllowed")
	public String getIsParcelShippingAllowed() {
		return IsParcelShippingAllowed;
	}
	public void setIsParcelShippingAllowed(String isParcelShippingAllowed) {
		IsParcelShippingAllowed = isParcelShippingAllowed;
	}
	@JsonProperty("IsDeliveryAllowed")
	public String getIsDeliveryAllowed() {
		return IsDeliveryAllowed;
	}
	public void setIsDeliveryAllowed(String isDeliveryAllowed) {
		IsDeliveryAllowed = isDeliveryAllowed;
	}
	@JsonProperty("AllowGiftWrap")
	public String getAllowGiftWrap() {
		return AllowGiftWrap;
	}
	public void setAllowGiftWrap(String allowGiftWrap) {
		AllowGiftWrap = allowGiftWrap;
	}
	@JsonProperty("CountryOfOrigin")
	public String getCountryOfOrigin() {
		return CountryOfOrigin;
	}
	public void setCountryOfOrigin(String countryOfOrigin) {
		CountryOfOrigin = countryOfOrigin;
	}
	@JsonProperty("TaxableFlag")
	public String getTaxableFlag() {
		return TaxableFlag;
	}
	public void setTaxableFlag(String taxableFlag) {
		TaxableFlag = taxableFlag;
	}
	@JsonProperty("UnitCost")
	public String getUnitCost() {
		return UnitCost;
	}
	public void setUnitCost(String unitCost) {
		UnitCost = unitCost;
	}
	@JsonProperty("DefaultProductClass")
	public String getDefaultProductClass() {
		return DefaultProductClass;
	}
	public void setDefaultProductClass(String defaultProductClass) {
		DefaultProductClass = defaultProductClass;
	}
	@JsonProperty("ItemType")
	public String getItemType() {
		return ItemType;
	}
	public void setItemType(String itemType) {
		ItemType = itemType;
	}
	@JsonProperty("IsReturnable")
	public String getIsReturnable() {
		return IsReturnable;
	}
	public void setIsReturnable(String isReturnable) {
		IsReturnable = isReturnable;
	}
	@JsonProperty("ColorCode")
	public String getColorCode() {
		return ColorCode;
	}
	public void setColorCode(String colorCode) {
		ColorCode = colorCode;
	}
	@JsonProperty("SizeCode")
	public String getSizeCode() {
		return SizeCode;
	}
	public void setSizeCode(String sizeCode) {
		SizeCode = sizeCode;
	}
	@JsonProperty("ImageID")
	public String getImageID() {
		return ImageID;
	}
	public void setImageID(String imageID) {
		ImageID = imageID;
	}
	@JsonProperty("ImageLocation")
	public String getImageLocation() {
		return ImageLocation;
	}
	public void setImageLocation(String imageLocation) {
		ImageLocation = imageLocation;
	}
	
	
}
