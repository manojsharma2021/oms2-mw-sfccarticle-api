package com.mnssfccarticle.model;

import java.io.Serializable;

import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SAPAttributes  implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	ClientAttributes clientAtrributes;
	ServerAttributes serverAtrributes;
}
