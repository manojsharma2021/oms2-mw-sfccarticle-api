package com.mnssfccarticle.model;

import java.io.Serializable;

import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ServerAttributes implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String repositoryDestination;
	private String destinationName;
	private String serverHost;
	private String messageServer;
	private String gwhost;
	private String gwserv;
	private String systemNumber;
	private String client;
	private String group;
	private String user;
	private String password;
	private String progid;
	private String port;

}
