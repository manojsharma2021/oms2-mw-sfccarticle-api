package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WBBDLD06_EXT {

	@JsonProperty("IDOC")
	private ArticleIDOC articleIdoc;

	public ArticleIDOC getIdoc() {
		return articleIdoc;
	}

	public void setIdoc(ArticleIDOC articleIdoc) {
		this.articleIdoc = articleIdoc;
	}
	
	
}
