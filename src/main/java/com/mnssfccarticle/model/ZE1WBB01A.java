package com.mnssfccarticle.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ZE1WBB01A {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("ATNAM")
	private String atnam;
	@JsonProperty("ATBEZ")
	private String atbez;
	@JsonProperty("ATWRT")
	private String atwrt;
	@JsonProperty("ATWTB")
	private String atwtb;
	@JsonProperty("RELEV")
	private String relev;
	@JsonProperty("PRICING")
	private String pricing;
}
