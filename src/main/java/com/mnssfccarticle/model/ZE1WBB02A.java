package com.mnssfccarticle.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ZE1WBB02A {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("BISMT")
	private String bismt;
	@JsonProperty("EXTWG")
	private String extwg;
	@JsonProperty("PLGTP")
	private String plgtp;
	@JsonProperty("COLOR")
	private String color;
	@JsonProperty("SIZE1")
	private String size1;
	@JsonProperty("FREE_CHAR")
	private String free_char;
	@JsonProperty("FASHGRD")
	private String fashgrd;
	@JsonProperty("BRAND_TYPE")
	private String brand_type;
	@JsonProperty("ZZSALES_PURCHASE")
	private String zzsales_purchase;
	@JsonProperty("ZZSEASONAL")
	private String zzseasonal;
	@JsonProperty("WHERL")
	private String wherl;
	@JsonProperty("ZVWARANPERD")
	private String zvwaranperd;
	@JsonProperty("MTPOS_MARA")
	private String mtpos_mara;
	@JsonProperty("ZCLASSID")
	private String zclassid;
	@JsonProperty("ZCLASSDESC")
	private String zclassdesc;
	@JsonProperty("ZFAMILY")
	private String zfamily;
	@JsonProperty("ZFAMILYDESC")
	private String zfamilydesc;
	@JsonProperty("ZSEGMENTDESC")
	private String zsegmentdesc;
	@JsonProperty("STEUC")
	private String steuc;
	@JsonProperty("SPART")
	private String spart;
	@JsonProperty("VTXTK")
	private String vtxtk;
	@JsonProperty("BRAND_ID")
	private String brand_id;
	@JsonProperty("WEKGR")
	private String wekgr;
	@JsonProperty("EKNAM")
	private String eknam;
	@JsonProperty("TRDRANGE")
	private String trdrange;
	@JsonProperty("TRDRANGE_SUB")
	private String trdrange_sub;
	@JsonProperty("CORE_RANGE")
	private String core_range;
	@JsonProperty("ZMERDESC")
	private String zmerdesc;
	@JsonProperty("GROES")
	private String groes;
	@JsonProperty("FLOW_NEW")
	private String flow_new;
}
