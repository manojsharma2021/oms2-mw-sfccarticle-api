package com.mnssfccarticle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ZE1WBB02B {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("MEINH")
	private String meinh;
	@JsonProperty("UMREZ")
	private String umrez;
	@JsonProperty("UMREN")
	private String umren;
	@JsonProperty("LAENG")
	private String laeng;
	@JsonProperty("BREIT")
	private String breit;
	@JsonProperty("HOEHE")
	private String hoehe;
	@JsonProperty("MEABM")
	private String meabm;
	@JsonProperty("VOLUM")
	private String volum;
	@JsonProperty("VOLEH")
	private String voleh;
	@JsonProperty("BRGEW")
	private String brgew;
	@JsonProperty("GEWEI")
	private String gewei;
	@JsonProperty("C_INFO_03")
	private String c_info_03;
	@JsonProperty("KZBSTME")
	private String kzbstme;
	@JsonProperty("KZAUSME")
	private String kzausme;
}
