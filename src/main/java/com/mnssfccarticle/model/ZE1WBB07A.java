package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ZE1WBB07A {
	

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("EAN11")
	private String ean11;
	@JsonProperty("EANTP")
	private String eantp;
	@JsonProperty("HPEAN")
	private String hpean;
	@JsonProperty("C_INFO_04")
	private String c_info_04;

}
