package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ZE1WBB09 {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("UNIT_CASE")
	private String unit_case;
	@JsonProperty("CMPR_QTY")
	private String cmpr_qty;
	@JsonProperty("CMPR_UNT")
	private String cmpr_unt;
	@JsonProperty("LBL_SIZE")
	private String lbl_size;
	@JsonProperty("PRINT_LABEL_FG")
	private String print_label_fg;
	@JsonProperty("LBL_QTY")
	private String lbl_qty;
	@JsonProperty("PLU_INDEX")
	private String plu_index;
	@JsonProperty("ZSHELFLABEL")
	private String zshelflabel;
	@JsonProperty("ZPOSDESC")
	private String zposdesc;
	
	@JsonProperty("MSG_CD")
	private String msg_cd;
	@JsonProperty("NON_MDSE_ID")
	private String non_mdse_id;
	@JsonProperty("QTY_RQRD_FG")
	private String qty_rqrd_fg;
	@JsonProperty("SLS_AUTH_FG")
	private String sls_auth_fg;
	@JsonProperty("FOOD_STAMP_FG")
	private String food_stamp_fg;
	@JsonProperty("NG_ENTRY_FG")
	private String ng_entry_fg;
	@JsonProperty("MAN_PRC_FG")
	private String man_prc_fg;
	@JsonProperty("WGT_ITM_FG")
	private String wgt_itm_fg;
	@JsonProperty("PRC_VRFY_FG")
	private String prc_vrfy_fg;
	@JsonProperty("INHBT_QTY_FG")
	private String inhbt_qty_fg;
	
	@JsonProperty("DCML_QTY_FG")
	private String dcml_qty_fg;
	@JsonProperty("RTN_CD")
	private String rtn_cd;
	@JsonProperty("WGT_SCALE_FG")
	private String wgt_scale_fg;
	@JsonProperty("POS_MSG")
	private String pos_msg;
	@JsonProperty("TAR_WGT_NBR")
	private String tar_wgt_nbr;
	@JsonProperty("CMPRTV_UOM")
	private String cmprtv_uom;
	@JsonProperty("PHARMACY_ITEM")
	private String pharmacy_item;
	@JsonProperty("DEA_GRP")
	private String dea_grp;
	@JsonProperty("BNS_BY_OPCODE")
	private String bns_by_opcode;
	@JsonProperty("BNS_BY_DESCR")
	private String bns_by_descr;
	
	@JsonProperty("COMP_TYPE")
	private String comp_type;
	@JsonProperty("SHELF_STOCK_FG")
	private String shelf_stock_fg;
	@JsonProperty("NO_MBR_CRD_PT_FG")
	private String no_mbr_crd_pt_fg;
	@JsonProperty("PRNT_PLU_RCPT_FG")
	private String prnt_plu_rcpt_fg;
	@JsonProperty("POS_RFND_MSG")
	private String pos_rfnd_msg;
	@JsonProperty("ASSUME_QTY_FG")
	private String assume_qty_fg;
	@JsonProperty("ZDEVICE_ID")
	private String zdevice_id;
	@JsonProperty("ZDEVICE_DESCR")
	private String zdevice_descr;
	@JsonProperty("POS_MSG2")
	private String pos_mg2;
	@JsonProperty("ZRED_DOT")
	private String zred_dot;
}
