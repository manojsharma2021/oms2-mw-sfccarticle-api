package com.mnssfccarticle.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ZE1WBB21 {
	
	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("AL_BOM_MATNR")
	private String al_bom_matnr;
	@JsonProperty("AL_BOM_QUANT")
	private String al_bom_quant;
	@JsonProperty("AL_BOM_UNIT")
	private String al_bom_unit;
	@JsonProperty("AL_BOM_MATNR_EXTERNAL")
	private String al_bom_matnr_external;
	@JsonProperty("AL_BOM_MATNR_VERSION")
	private String al_bom_matnr_version;
	@JsonProperty("AL_BOM_MATNR_GUID")
	private String al_bom_matnr_guid;
	@JsonProperty("AL_BOM_EAN")
	private String al_bom_ean;
}
