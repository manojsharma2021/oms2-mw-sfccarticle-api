package com.mnssfccarticle.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ZE1WST01 {

	@JsonProperty("SEGMENT")
	private String segment;
	@JsonProperty("REGIO")
	private String regio;
}
