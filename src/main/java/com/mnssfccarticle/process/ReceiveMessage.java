package com.mnssfccarticle.process;
 
import javax.jms.Message;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsOperations;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;
import org.springframework.stereotype.Component;
 
 //The class of the message consumer must add @Component, or @Service, in this case, the message consumer class will be delegated to the Listener class, the principle is similar to the use of SessionAwareMessageListener and MessageListenerAdapter to implement message-driven POJO
@Component
public class ReceiveMessage extends MessageListenerAdapter{
	@Autowired
	JmsOperations jmsOperations;
 
   /* @Override
    @JmsListener(destination = "Q.OMS.ENRICHEDITEMFEED.IN")
    public void onMessage(Message message) {
        String messageBody = new String(message.toString());
    	     System.out.println("Successfully monitor the Q1 message queue, the value is: "+ messageBody);
    }*/
}