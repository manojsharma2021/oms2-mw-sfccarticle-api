package com.mnssfccarticle.process;
 
import javax.annotation.PostConstruct;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsOperations;
import org.springframework.stereotype.Component;
 
@Component
public class SendMessage {
	@Autowired
	JmsOperations jmsOperations;
 
        //@PostConstruct runs when the server loads the Servle, and will only be executed once by the server, @PreDestroy is executed after the execution of the destroy() method
	//@PostConstruct
	public void send(){
		try {
		 System.out.println("Start sending messages");
		 String productXMLMsg = "<Item Action=\"Manage\" GlobalItemID=\"TestItem15\" IsSyncRequired=\"Y\" ItemGroupCode=\"PROD\" ItemID=\"TestItem15\" OrganizationCode=\"MNS7\" UnitOfMeasure=\"EACH\"> <PrimaryInformation AllowGiftWrap=\"N\" ColorCode=\"Grey\" CountryOfOrigin=\"\" DefaultProductClass=\"GOOD\" Description=\"M CASUAL T'S &amp; POLOS\" ExtendedDescription=\"\" IsDeliveryAllowed=\"Y\" IsParcelShippingAllowed=\"Y\" IsProcurementAllowed=\"Y\" IsReturnable=\"Y\" IsShippingAllowed=\"Y\" ItemType=\"\" ProductLine=\"Menswear\" ReturnWindow=\"\" ShortDescription=\"\" SizeCode=\"M\" Status=\"3000\" TaxableFlag=\"\" UnitCost=\"\"/> <InventoryParameters ATPRule=\"ATP\" InventoryMonitorRule=\"MNS_INV_RULE\"/> <ClassificationCodes TaxProductCode=\"6105\"/> <ItemAliasList> <ItemAlias Action=\"Modify\" AliasName=\"EAN\" AliasValue=\"TestItem5\" ItemKey=\"20210510160602266031\"/> <ItemAlias Action=\"Modify\" AliasName=\"GSTIN\" AliasValue=\"12345\" ItemKey=\"20210510160602266031\"/> </ItemAliasList> </Item>";

	       jmsOperations.convertAndSend("TEST_QUEUE_1",productXMLMsg);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}