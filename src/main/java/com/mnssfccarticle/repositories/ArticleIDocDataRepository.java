package com.mnssfccarticle.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.mnssfccarticle.model.E1WBB01;



@Repository
public interface ArticleIDocDataRepository extends MongoRepository<E1WBB01, Integer>{

	E1WBB01 findByMatnr(String matnr);
	
	List<E1WBB01> findBySfccstatus(int sfccstatus);
	Page<E1WBB01> findBySfccstatus(int sfccstatus, Pageable pageable);

}
