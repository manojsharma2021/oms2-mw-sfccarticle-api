package com.mnssfccarticle.repositories;



import org.springframework.data.repository.CrudRepository;

import com.mnssfccarticle.model.SfccArt;

public interface SfccArtRepository extends CrudRepository<SfccArt, Long> {
	SfccArt findByTitle(String title);
}