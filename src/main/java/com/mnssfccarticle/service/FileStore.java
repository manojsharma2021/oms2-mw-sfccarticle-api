package com.mnssfccarticle.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.mnssfccarticle.model.CsvBean;
import com.mnssfccarticle.model.E1WBB01;
import com.mnssfccarticle.model.SfccArt;
import com.mnssfccarticle.repositories.ArticleIDocDataRepository;

import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
@Service
public class FileStore {
    private final AmazonS3 amazonS3;

    @Autowired
    ArticleIDocDataRepository articleRepository;
    public void upload(String path,
                       String fileName,
                       Optional<Map<String, String>> optionalMetaData,
                       InputStream inputStream) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        optionalMetaData.ifPresent(map -> {
            if (!map.isEmpty()) {
                map.forEach(objectMetadata::addUserMetadata);
            }
        });
        try {
            amazonS3.putObject(path, fileName, inputStream, objectMetadata);
        } catch (AmazonServiceException e) {
            throw new IllegalStateException("Failed to upload the file", e);
        }
    }

    public boolean download(String path,String key) {
        try {
          //  S3Object object = amazonS3.getObject(path, key);
        	ObjectListing list = amazonS3.listObjects(path, key);
        			 List<S3ObjectSummary> objectSummaries = list.getObjectSummaries();
        			 
            for (S3ObjectSummary objectSummary : objectSummaries) {
                if (objectSummary.getKey().contains(".csv")) {
                	S3Object object = amazonS3.getObject(path, objectSummary.getKey());
                	InputStream inputStream=object.getObjectContent();
                	//IOUtils.copy(inputStream, new FileOutputStream(new File("C:\\Mark&Spencer\\"+objectSummary.getETag()+".csv")));
                	CsvMapper csvMapper = new CsvMapper();
                	csvMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
                        	MappingIterator<CsvBean> csvBeanIter = csvMapper.readerWithTypedSchemaFor(CsvBean.class).readValues(inputStream);
                        	List<CsvBean> csvBeans = csvBeanIter.readAll();
                        	//System.out.println(csvBeans.get(101).toString());
                        	for(CsvBean csvBean : csvBeans)
                        	{
                        		E1WBB01 e1wbb01 = articleRepository.findByMatnr(csvBean.getArticle_ID());
                        		if(e1wbb01==null)
                        		e1wbb01=articleRepository.findByMatnr("0000000"+csvBean.getArticle_ID());
                        		if(e1wbb01!=null)
                        		{
                        			String image_link = csvBean.getImage_Link();
                        			String name = image_link.substring(image_link.lastIndexOf('/') + 1);
                        			String location = image_link.substring(0,image_link.lastIndexOf('/') );
                        			e1wbb01.setImageLink(location);
                        			e1wbb01.setImageId(name);
                        			e1wbb01.setSize(csvBean.getSize());
                        			e1wbb01.setColor(csvBean.getColor());
                        			e1wbb01.setSfccstatus(0);
                        			articleRepository.save(e1wbb01);
                        		}
                        	}
                   /* data = downloadFile(s3Client, bucketName, objectSummary.getKey());
                    fileName = URLEncoder.encode(objectSummary.getKey(), "UTF-8").replaceAll("\\+", "%20");
                    System.out.println(fileName);*/
                }
            }
           return true;
        } catch (AmazonServiceException | IOException e) {
            throw new IllegalStateException("Failed to download the file", e);
        }
    }

}