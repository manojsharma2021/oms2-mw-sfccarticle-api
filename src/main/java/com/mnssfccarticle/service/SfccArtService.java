package com.mnssfccarticle.service;

import com.mnssfccarticle.model.SfccArt;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface SfccArtService {
	SfccArt saveTodo(String title, String description, MultipartFile file);

    boolean downloadCSV();

    List<SfccArt> getAllTodos();
    String postSfccToMQ(Pageable pageable);
    String pushArticleByMatnr(String[] matnr);
}
