package com.mnssfccarticle.service;

import com.mnssfccarticle.config.BucketName;
import com.mnssfccarticle.model.SfccArt;
import com.mnssfccarticle.repositories.ArticleIDocDataRepository;
import com.mnssfccarticle.repositories.SfccArtRepository;
import com.mnssfccarticle.task.ArticleTask;
import com.mnssfccarticle.task.MatnrTask;

import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.jms.core.JmsOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import static org.apache.http.entity.ContentType.*;

@Service
@AllArgsConstructor
public class SfccArtServiceImpl implements SfccArtService {
    private final FileStore fileStore;
    private final SfccArtRepository repository;
	@Autowired
	JmsOperations jmsOperations;
	
	 @Autowired
	    ArticleIDocDataRepository articleRepository;

	public String postSfccToMQ(Pageable pageable)
	{
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
		
		 ArticleTask task = new ArticleTask(pageable,articleRepository,jmsOperations);
			executor.execute(task);
		
			//break;
		//}
		
		return "success";
	}
	
    @Override
    public SfccArt saveTodo(String title, String description, MultipartFile file) {
        //check if the file is empty
        if (file.isEmpty()) {
            throw new IllegalStateException("Cannot upload empty file");
        }
        //Check if the file is an image
        if (!Arrays.asList(IMAGE_PNG.getMimeType(),
                IMAGE_BMP.getMimeType(),
                IMAGE_GIF.getMimeType(),
                IMAGE_JPEG.getMimeType()).contains(file.getContentType())) {
            throw new IllegalStateException("FIle uploaded is not an image");
        }
        //get file metadata
        Map<String, String> metadata = new HashMap<>();
        metadata.put("Content-Type", file.getContentType());
        metadata.put("Content-Length", String.valueOf(file.getSize()));
        //Save Image in S3 and then save Todo in the database
        String path = String.format("%s/%s", BucketName.TODO_IMAGE.getBucketName(), UUID.randomUUID());
        String fileName = String.format("%s", file.getOriginalFilename());
        try {
            fileStore.upload(path, fileName, Optional.of(metadata), file.getInputStream());
        } catch (IOException e) {
            throw new IllegalStateException("Failed to upload file", e);
        }
        SfccArt todo = SfccArt.builder()
                .description(description)
                .title(title)
                .imagePath(path)
                .imageFileName(fileName)
                .build();
        repository.save(todo);
        return repository.findByTitle(todo.getTitle());
    }

    @Override
    public boolean downloadCSV() {
    	 String path = String.format("%s", BucketName.TODO_IMAGE.getBucketName());
      // return fileStore.download(path,"rra-feed/rra-catalog/oms2-input/");
       return fileStore.download(path,"rra-feed/rra-catalog/input/");
    }

    @Override
    public List<SfccArt> getAllTodos() {
        List<SfccArt> todos = new ArrayList<>();
        repository.findAll().forEach(todos::add);
        return todos;
    }

	@Override
	public String pushArticleByMatnr(String[] matnr) {
		// TODO Auto-generated method stub
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
		
		 MatnrTask task = new MatnrTask(matnr,articleRepository,jmsOperations);
			executor.execute(task);
		
			//break;
		//}
		
		return "success";
	}
}
