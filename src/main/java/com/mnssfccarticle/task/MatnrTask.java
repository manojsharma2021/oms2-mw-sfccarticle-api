package com.mnssfccarticle.task;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.jms.core.JmsOperations;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.mnssfccarticle.model.ClassificationCodes;
import com.mnssfccarticle.model.E1WBB01;
import com.mnssfccarticle.model.E1WBB04;
import com.mnssfccarticle.model.Extn;
import com.mnssfccarticle.model.InventoryParameters;
import com.mnssfccarticle.model.Item;
import com.mnssfccarticle.model.ItemAlias;
import com.mnssfccarticle.model.ItemAliasList;
import com.mnssfccarticle.model.ItemList;
import com.mnssfccarticle.model.PrimaryInformation;
import com.mnssfccarticle.repositories.ArticleIDocDataRepository;
import com.mnssfccarticle.util.IConstant;


public class MatnrTask implements Runnable{
	Pageable pageable;
	JmsOperations jmsOperations;
	String[] matnr;
	ArticleIDocDataRepository articleRepository;
	private final static Logger LOGGER = LoggerFactory.getLogger(ArticleTask.class);
	
	
	public MatnrTask(String[] matnr, ArticleIDocDataRepository articleRepository, JmsOperations jmsOperations) {
		// TODO Auto-generated constructor stub
		super();
		this.matnr = matnr;
		this.articleRepository=articleRepository;
		this.jmsOperations=jmsOperations;
	}

	

	public void run() {
		// TODO Auto-generated method stub
		try {
			int count=0;
			LOGGER.info("Total Items. "+matnr.length);
			
			for(String matnrVal : matnr) {
			E1WBB01 e1wbb01 = articleRepository.findByMatnr(matnrVal);
			if(e1wbb01==null)
				e1wbb01=articleRepository.findByMatnr("0000000"+matnrVal);
			if(e1wbb01==null)
			{
				LOGGER.info("Record couldn't be found for  "+matnrVal);
				continue;
			}
			if(e1wbb01.getSfccstatus()!=0)
				continue;
				count=count+1;
				LOGGER.info("Initiating Process for Item No. "+count);
				ArrayList<Item> items = new ArrayList<Item>();
					String ExtnIsOnSale="N";
					if(e1wbb01.getZe1wbb01a().get(0).getAtwrt().equalsIgnoreCase("01"))
						ExtnIsOnSale="Y";
					Extn Extn = new Extn(e1wbb01.getE1wbb02().getZe1wbb02a().getSteuc(), e1wbb01.getE1wbb02().getZe1wbb02a().getBismt(), e1wbb01.getE1wbb03().getE1wbb07().get(0).getE1wbb08().getKwert(), ExtnIsOnSale, e1wbb01.getE1wbb02().getZe1wbb02a().getPlgtp(), e1wbb01.getE1wbb02().getZe1wbb02a().getSpart(), e1wbb01.getE1wbb02().getZe1wbb02a().getZclassid(), e1wbb01.getE1wbb02().getZe1wbb02a().getZclassdesc(), e1wbb01.getE1wbb02().getZe1wbb02a().getZfamily(), e1wbb01.getE1wbb02().getZe1wbb02a().getZfamilydesc(), e1wbb01.getE1wbb02().getZe1wbb02a().getZsegmentdesc(), e1wbb01.getE1wbb02().getZe1wbb02a().getZmerdesc(), e1wbb01.getE1wbb02().getZe1wbb02a().getBrand_id(), e1wbb01.getE1wbb02().getZe1wbb02a().getWekgr(), e1wbb01.getE1wbb02().getZe1wbb02a().getEknam(), e1wbb01.getE1wbb02().getZe1wbb02a().getTrdrange(), e1wbb01.getE1wbb02().getZe1wbb02a().getTrdrange_sub(), e1wbb01.getE1wbb02().getZe1wbb02a().getCore_range(), "", "", "");
					//PrimaryInformation primaryInformation = new PrimaryInformation(IConstant.primaryInformationStatus, "", e1wbb01.getE1wbb02().getZe1wbb02a().getZmerdesc(), e1wbb01.getE1wbb02().getZe1wbb02a().getZmerdesc(), e1wbb01.getE1wbb02().getMtart(), e1wbb01.getE1wbb02().getZe1wbb02a().getVtxtk(), IConstant.flagYes, IConstant.flagYes, IConstant.flagYes, IConstant.defaultProductClass, IConstant.flagYes, e1wbb01.getE1wbb02().getZe1wbb02a().getWherl(), IConstant.flagYes, e1wbb01.getE1wbb03().getE1wbb07().get(0).getE1wbb08().getKwert(), DefaultProductClass, ItemType, IsReturnable, ColorCode, SizeCode, ImageID, ImageLocation)
					PrimaryInformation primaryInformation1 = new PrimaryInformation(IConstant.primaryInformationStatus, e1wbb01.getE1wbb10().getMaktm() , e1wbb01.getE1wbb02().getZe1wbb02a().getZmerdesc(), e1wbb01.getE1wbb02().getZe1wbb02a().getZmerdesc(), "", e1wbb01.getE1wbb02().getZe1wbb02a().getVtxtk(), IConstant.flagYes, IConstant.flagYes, IConstant.flagYes, IConstant.flagYes, IConstant.flagYes, e1wbb01.getE1wbb02().getZe1wbb02a().getWherl(), IConstant.flagYes, e1wbb01.getE1wbb03().getE1wbb07().get(0).getE1wbb08().getKwert(), IConstant.primaryInformationStatus, e1wbb01.getE1wbb02().getMtart(), IConstant.flagYes, e1wbb01.getColor(), e1wbb01.getSize(), e1wbb01.getImageId(), e1wbb01.getImageLink());
					InventoryParameters inventoryParameters = new InventoryParameters(IConstant.atpRule, IConstant.inventoryMonitorRule);
					ClassificationCodes classificationCodes = new ClassificationCodes(e1wbb01.getE1wbb02().getZe1wbb02a().getSteuc());
					List<E1WBB04> e1wbb04s = e1wbb01.getE1wbb03().getE1wbb04();
					List<ItemAlias> itemAliasLi= new ArrayList<ItemAlias>();
					
					for(E1WBB04 e1wbb04 : e1wbb04s) {
					ItemAlias itemAlias;
					if(e1wbb04.getEantp().equalsIgnoreCase("HE"))
					itemAlias = new ItemAlias(IConstant.itemAliasAction, IConstant.itemAliasNameGSTIN, e1wbb04.getEan11());
					else
						itemAlias = new ItemAlias(IConstant.itemAliasAction, IConstant.itemAliasName, e1wbb04.getEan11());
					
					itemAliasLi.add(itemAlias);
					}
					ItemAliasList itemAliasList = new ItemAliasList(itemAliasLi);
					Item item = new Item(Extn, primaryInformation1, inventoryParameters, classificationCodes, itemAliasList, e1wbb01.getMatnr(), IConstant.UnitofMeasure, IConstant.organizationCode, IConstant.itemGroupCode, IConstant.itemAction, e1wbb01.getE1wbb03().getE1wbb04().get(0).getEan11());
					items.add(item);
				
				ItemList itemList = new ItemList(items);
				XmlMapper xmlMapper = new XmlMapper();
				try {
					String articleXml = xmlMapper.writeValueAsString(itemList);
					LOGGER.info(articleXml);
					//jmsOperations.convertAndSend("Q.OMS.ITEMFEED.IN",articleXml);
					//CSV
					jmsOperations.convertAndSend("Q.OMS.ITEMFEED.IN",articleXml);
					e1wbb01.setStatus("1");
					articleRepository.save(e1wbb01);
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					LOGGER.info("JsonProcessingException");
				}
				//Thread.sleep(3000);
				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOGGER.info("Exception");
		}
	}
	
}
