package com.mnssfccarticle.util;

public class IConstant {

	/*
	 * Processing Status Values
	 * 1 == Processing
	 * 2 == SAP RFC Success
	 * 3 == SAP RFC Failure
	 * 4 == SAP IDOC
	 * 5 == Dispatched
	 */
	public static final String primaryInformationStatus="3000";
	public static final String flagYes="Y";
	public static final String flagNo="N";
	public static final String defaultProductClass="GOOD";
	public static final String atpRule="ATP";
	public static final String inventoryMonitorRule="MNS_INV_RULE";
	public static final String itemAliasAction="Modify";
	public static final String itemAliasName="EAN";
	public static final String itemAliasNameGSTIN="GSTIN";
	public static final String organizationCode="MNS";
	public static final String UnitofMeasure="EACH";
	public static final String itemGroupCode="PROD";
	public static final String itemAction = "Manage";
	public static String environment="";
}
